import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
    ],
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url))
        }
    },
    server: {
        cors: true,
        open: false,
        port: 5177,
        proxy: {
            //     '/tag-classification': {
            //         target: 'http://localhost:8600',
            //         changeOrigin: true,
            //         ws: true,
            //         rewrite: (path) => path.replace(/^\/tag-classification/, '')
            //     },
            //     '/customer': {
            //         target: 'http://localhost:8400',
            //         changeOrigin: true,
            //         ws: true,
            //         rewrite: (path) => path.replace(/^\/customer/, '')
            //     },
            //     '/course-article': {
            //         target: 'http://localhost:8300',
            //         changeOrigin: true,
            //         ws: true,
            //         rewrite: (path) => path.replace(/^\/course-article/, '')
            //     },
            //     '/comment': {
            //         target: 'http://localhost:8200',
            //         changeOrigin: true,
            //         ws: true,
            //         rewrite: (path) => path.replace(/^\/comment/, '')
            //     },
            '/api': {
                target: 'http://localhost:9999',
                changeOrigin: true,
                ws: true,
                rewrite: (path) => path.replace(/^\/api/, '')
            },
        },
        host: "0.0.0.0"
    }
})
