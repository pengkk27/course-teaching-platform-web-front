import axios from 'axios'

const request = axios.create({
	// baseURL: "",
	timeout: 5000,//超时时间
	headers: {//编译语言
		"Content-type": "application/json;charset=utf-8"
	}
})

request.interceptors.request.use((config: any) => {
	config.headers.Authorization = `${window.sessionStorage.getItem('token')}`
	return config
})

export default request