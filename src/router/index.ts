import { createRouter, createWebHistory } from 'vue-router'
import IndexView from '../views/IndexView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: IndexView,
      redirect: '/index',
      children: [
        {
          path: '/index',
          name: 'index',
          meta: {
            title: '计算机学习平台'
          },
          component: () => import ('../components/common/IndexBodyComponent.vue')
        },
        {
          path: '/search',
          name: 'search',
          meta: {
            title: '搜索'
          },
          component: () => import('../views/search/SearchView.vue')
        },
        {
          path: '/teacherDetail',
          name: 'teacherDetail',
          meta: {
            title: '教师详情'
          },
          component: () => import('../views/teacher/TeacherDetailView.vue')
        },
        {
          path: '/category',
          name: 'category',
          meta: {
            title: '分类'
          },
          component: () => import('../views/category/CategoryView.vue')
        },
        {
          path: '/courseDetail',
          name: 'courseDetail',
          meta: {
            title: '课程详情'
          },
          component: () => import('../views/course/CourseDetailView.vue')
        },
        {
          path: '/section',
          name: 'section',
          meta: {
            title: '章节'
          },
          component: () => import('../views/course/SectionView.vue')
        },
        {
          path: '/articleDetail',
          name: 'articleDetail',
          meta: {
            title: '文章详情'
          },
          component: () => import('../views/article/ArticleDetailView.vue')
        },
        {
          path: '/studentPersonal',
          name: 'studentPersonal',
          meta: {
            title: '学生个人信息'
          },
          component: () => import('../views/student/StudentPersonalView.vue')
        },
        {
          path: '/studentPersonalChangeInfo',
          name: 'studentPersonalChangeInfo',
          meta: {
            title: '修改信息'
          },
          component: () => import('../views/student/ChangePersonalInfo.vue')
        },
        {
          path: '/teacherAdmin',
          name: 'teacherAdmin',
          meta: {
            title: '教师个人界面'
          },
          component: () => import('../views/teacher/TeacherAdminView.vue')
        },
        {
          path: '/createCourse',
          name: 'createCourse',
          meta: {
            title: '创建课程'
          },
          component: () => import('../views/teacher/CreateCourseView.vue')
        },
        {
          path: '/teacherCourseDetail',
          name: 'teacherCourseDetail',
          meta: {
            title: '课程详情'
          },
          component: () => import('../views/teacher/TeacherCourseDetailView.vue')
        },
        {
          path: '/createSection',
          name: 'createSection',
          meta: {
            title: '创建章节'
          },
          component: () => import('../views/teacher/CreateSectionView.vue')
        },
        {
          path: '/createArticle',
          name: 'createArticle',
          meta: {
            title: '创建文章'
          },
          component: () => import('../views/teacher/CreateArticleView.vue')
        },
        {
          path: '/allArticle',
          name: 'allArticle',
          meta: {
            title: '全部文章'
          },
          component: () => import('../views/article/AllArticleView.vue')
        },
        {
          path: '/allCourse',
          name: 'allCourse',
          meta: {
            title: '全部文章'
          },
          component: () => import('../views/course/AllCourseView.vue')
        },
        {
          path: '/personalInformation',
          name: 'personalInformation',
          meta: {
            title: '个人信息'
          },
          redirect: '/personalInformation/avatar',
          component: () => import('../views/student/PersonalInformation.vue'),
          children: [
            {
              path: '/personalInformation/avatar',
              name: 'avatar',
              component: () => import('../components/personal/PersonalAvatarComponent.vue')
            },
            {
              path: '/personalInformation/information',
              name: 'information',
              component: () => import('../components/personal/PersonalInformationComponent.vue')
            },
            {
              path: '/personalInformation/password',
              name: 'password',
              component: () => import('../components/personal/PersonalPasswordComponent.vue')
            },
            {
              path: '/personalInformation/cancellation',
              name: 'cancellation',
              component: () => import('../components/personal/PersonalCancellationComponent.vue')
            },
          ]
        },
      ]
    },
    {
      path: '/login',
      name: 'login',
      meta: {
        title: '登录'
      },
      component: () => import('../views/login/LoginView.vue')
    },
    {
      path: '/register',
      name: 'register',
      meta: {
        title: '学生注册'
      },
      component: () => import('../views/login/RegisterView.vue')
    },
    {
      path: '/findPassword',
      name: 'findPassword',
      meta: {
        title: '找回密码'
      },
      component: () => import('../views/login/ForgetPasswordView.vue')
    },
  ]
})

router.beforeEach((to, from, next) => {
  if(to.meta.title) {
    document.title = to.meta.title.toString()
  }
  next()
  // const token = window.sessionStorage.getItem('token')
  // if (to.path == '/' || to.path == '/login' || to.path =='/index'
  //     || to.path == '/register'
  //     || to.path == '/category'
  // ) return next()
  // if (!token) {
  //   next('/')
  // } else {
  //   next()
  // }
})

export default router
