import { defineStore } from 'pinia'
import { ref } from 'vue'

export const searchStore = defineStore('search',
  () => {
    const searchType = ref('')
    const searchKeyword = ref('')

    const setSearchInfo = (type: string, keyword: string) => {
      searchType.value = type
      searchKeyword.value = keyword
    }

    return { searchType, searchKeyword, setSearchInfo }
  },
  {
    persist: true,
  }
)
