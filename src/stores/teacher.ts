import {defineStore} from 'pinia'
import {ref} from 'vue'

export const teacherStore = defineStore('teacher',
    () => {
        const teacherId = ref('')
        const teacherName = ref('')
        const teacherImage = ref('')
        return {teacherId, teacherName, teacherImage}
    },
    {
        persist: true,
    }
)
