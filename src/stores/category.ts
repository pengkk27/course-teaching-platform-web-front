import { defineStore } from 'pinia'
import { ref } from 'vue'

export const categoryStore = defineStore('category',
  () => {
    const categoryId = ref('')

    return { categoryId }
  },
  {
    persist: true,
  }
)
