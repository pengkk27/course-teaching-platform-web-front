import { defineStore } from 'pinia'
import { ref } from 'vue'

export const articleStore = defineStore('article',
  () => {
    const articleId = ref('')
    const articleName = ref('')

    return { articleId, articleName }
  },
  {
    persist: true,
  }
)
