import {defineStore} from 'pinia'
import {ref} from 'vue'

/**
 * 定义课程数据接口
 */
interface Course {
    id: string
    courseImg: string
    courseName: string
    teacherName: string
    classificationName: string
    tagName: Array<string>
    courseDescription: string
}


export const courseStore = defineStore('course',
    () => {
        const course = ref<Course>({
            id: '',
            courseImg: '',
            courseName: '',
            teacherName: '',
            classificationName: '',
            tagName: [],
            courseDescription: ''
        })

        const createCourseId = ref('')
        return {course, createCourseId}
    },
    {
        persist: true,
    }
)
