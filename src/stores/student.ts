import { defineStore } from 'pinia'
import { ref } from 'vue'

export const studentStore = defineStore('student',
  () => {
    const studentId = ref('')
    const studentName = ref('')
    const studentImage = ref('')
    const studentEmail = ref('')

    return { studentId, studentName, studentImage, studentEmail }
  },
  {
    persist: true,
  }
)
