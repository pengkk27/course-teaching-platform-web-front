import {defineStore} from 'pinia'
import {ref} from 'vue'

export const sectionStore = defineStore('section',
    () => {
        const sectionId = ref('')
        const sectionName = ref('')
        const sectionImage = ref('')

        return {sectionId, sectionName, sectionImage}
    },
    {
        persist: true,
    }
)
