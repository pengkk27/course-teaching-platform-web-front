import { getAllCourseApi, getRecommendedCourseApi } from "@/apis/course"
import { ElMessage } from "element-plus"
import { onMounted, ref } from "vue"
import { studentStore } from "@/stores/student"

// 课程接口
interface Course {
    id: string
    courseName: string
    teacherName: string
    courseImg: string
    classificationName: string
    tagName: Array<any>
    courseDescription: string
    status: string
    createTime: string
    updateTime: string
}

export default function () {

    const allCourse = ref<Course[]>([])

    const carouselCourse = ref<Course[]>([])

    const popularCourse = ref<Course[]>([])

    const recommendedCourse = ref<Course[]>([])

    const studentId = studentStore().studentId

    function getAllCourse() {
        getAllCourseApi().then((res) => {
            let data = res.data
            if (data.code !== 200) {
                ElMessage.error('操作失败')
                return
            }

            allCourse.value = data.result.filter((item: Course) => {
                return (item.status === '2')
            })

            for (let i = 0; i < 3; i++) {
                carouselCourse.value.push(allCourse.value[i])
            }

            for (let i = 0; i < 4; i++) {
                popularCourse.value.push(allCourse.value[i])
            }
        })
    }

    function getRecommendedCourse() {
        getRecommendedCourseApi(studentId).then((res) => {
            let data = res.data
            if (data.code !== 200) {
                ElMessage.error('操作失败')
                return
            }

            const temp = ref<Course[]>([])
            temp.value = data.result.filter((item: Course) => {
                return (item.status === '2')
            })


            if (data.result.length < 4) {
                for (let i = 0; i < data.result.length; i++) {
                    recommendedCourse.value.push(temp.value[i])
                }
            } else {
                for (let i = 0; i < 4; i++) {
                    recommendedCourse.value.push(temp.value[i])
                }
            }


            if (recommendedCourse.value.length === 0) {
                recommendedCourse.value = popularCourse.value
            }
        })
    }


    onMounted(() => {
        getAllCourse()
        if (studentId !== '') {
            getRecommendedCourse()
        }
    })

    return { allCourse, carouselCourse, popularCourse, recommendedCourse }
}