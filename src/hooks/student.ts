import { ref } from 'vue'

/**
 * 用户接口
 */
interface User {
    id: string
    username: string
    realName: string
    password: string
    image: string
    phone: string
    roleId: string
    introduction: string
    effect: string
    createTime: string
    updateTime: string
}

