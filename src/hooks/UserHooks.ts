import { sendEmailApi, resetPasswordApi } from "@/apis/student"
import { ElMessage } from "element-plus"

export default function () {

    async function sendEmail(email: string) {
        const res = await sendEmailApi(email)
        let data = res.data
        if (data.code !== 200) {
            ElMessage.error('发送失败')
            return false
        }
        if (!data.result) {
            ElMessage.error('该邮箱没有注册过账号')
            return false
        } else {
            ElMessage.success('验证码邮件发送成功，注意查看邮箱')
            return true
        }
    }

    async function resetPassword(email: string, code: string) {
        const res = await resetPasswordApi(email, code)
        let data = res.data
        if (data.code !== 200) {
            ElMessage.error('发送失败')
            return false
        }
        if (!data.result) {
            ElMessage.error('验证失败')
            return false
        } else {
            ElMessage.success('验证成功，已发送临时密码到邮箱，登陆后请尽快修改！')
            return true
        }
    }

    return { sendEmail, resetPassword }
}