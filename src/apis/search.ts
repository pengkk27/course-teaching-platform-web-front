import request from '@/utils/request'

//######################################################################
/******************************* 搜索接口 *******************************/

/**
 * 请求搜索接口
 * @param data
 * @returns
 */
export const searchApi = (data: object) => {
    return request({
        url: '/api/category/getCategoryList',
        method: 'get',
        data
    })
}
