import request from '@/utils/request'

//######################################################################
/******************************* 标签接口 *******************************/

/**
 * 获取标签列表接口
 * @returns 标签列表数组
 * 返回的内容为两个：
 * {
 *  id: string
 *  name: string
 * }
 */
export const getTagListApi = () => {
    return request({
        url: '/api/tag-classification/tag/getAllTag',
        method: 'get'
    })
}
