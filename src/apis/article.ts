import request from '@/utils/request'

//######################################################################
/******************************* 文章接口 *******************************/

/**
 * 获取搜索的文章列表
 * @returns 课程的列表
 * 返回的课程数据内容：
 * {
 *  id: string
 *  title: string
 *  author: string
 *  tags: Array<string>
 *  createTime: string
 * }
 */
export const getAllArticleApi = () => {
    return request({
        url: '/api/course-article/article/getAllArticle',
        method: 'get'
    })
}

/**
 * 根据文章ID获取文章的内容
 * @param data 传过来的文章ID
 */
export const getArticleDetailByIdApi = (id: string) => {
    return request({
        url: '/api/course-article/article/getArticleDetailById?id=' + id,
        method: 'get'
    })
}

/**
 * 根据学生ID获取收藏的文章列表
 * @returns 文章的列表
 * 返回的文章数据内容：
 * {
 *  id: string
 *  title: string
 *  author: string
 *  tags: Array<string>
 *  createTime: string
 * }
 */
export const getCollectArticleByStudentIdApi = (studentId: string) =>{
    return request({
        url: '/api/course-article/article/getCollectArticleByStudentId?id=' + studentId,
        method: 'get'
    })
}

/**
 * 根据教师ID查询该教师的文章
 * @param teacherId 教师的ID
 */
export const getArticleByTeacherIdApi = (teacherId: string) => {
    return request({
        url: '/api/course-article/article/getArticleByTeacherId?id=' + teacherId,
        method: 'get'
    })
}


/**
 * 创建文章接口
 * @param data 创建的文章的信息
 * @returns 正常的返回
 */
export const createArtile = (data: any) => {
    return request({
        url: '/api/course-article/article/addArticle',
        method: 'POST',
        data
    })
}

/**
 * 判断学生是否收藏了课程
 * @param articleId 文章ID
 * @param studentId 学生ID
 * @returns 布尔类型，是否收藏了课程
 */
export const isTheArticleCollectedApi = (articleId: string, studentId: string) => {
    return request({
        url: '/api/course-article/article/judgeCollectArticleByStudentId?articleId=' + articleId + '&studentId=' + studentId,
        method: 'GET'
    })
}

/**
 * 将文章添加到学生收藏
 * @param studentId 学生ID
 * @param articleId 文章ID
 */
export const addCollectArticleByStudentIdApi = (studentId: string, articleId: string) =>{
    return request({
        url: '/api/course-article/article/addCollectArticleByStudentId?studentId=' + studentId + '&article=' + articleId,
        method: 'get'
    })
}

/**
 * 将文章取消学生收藏
 * @param studentId 学生ID
 * @param articleId 文章ID
 */
export const removeCollectArticleByStudentIdApi = (studentId: string, articleId: string) =>{
    return request({
        url: '/api/course-article/article/removeCollectArticleByStudentId?articleId=' + articleId + '&studentId=' + studentId,
        method: 'GET'
    })
}