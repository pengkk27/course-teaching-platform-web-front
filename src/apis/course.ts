import request from '@/utils/request'

//######################################################################
/******************************* 课程接口 *******************************/

/**
 * 获取搜索的课程列表
 * @returns 课程的列表
 * 返回的课程数据内容：
 * {
 *  id: string
 *  name: string
 *  img: string
 *  author: string
 *  category: string
 *  tags: Array<string>
 *  describe: string
 * }
 */
export const getAllCourseApi = () => {
    return request({
        url: '/api/course-article/course/getAllCourse',
        method: 'GET'
    })
}

/**
 * 根据分类的ID获取该分类下的所有课程
 * @returns 返回课程列表，列表内容和搜索返回的列表内容一致
 * 返回的课程数据内容：
 * {
 *  id: string
 *  name: string
 *  img: string
 *  author: string
 *  category: string
 *  tags: Array<string>
 *  describe: string
 * }
 * @param classificationId
 */
export const getCourseByCategoryId = (classificationId: string) => {
    return request({
        url: '/api/course-article/course/queryAllByCategoryId?classificationId=' + classificationId,
        method: 'get',
    })
}

/**
 * 根据课程的ID来查询所属这个课程的节
 * @param data 课程的ID
 * @returns 返回课程节的列表
 * 返回的数据内容为：
 * [
 *  {
 *      id: string
 *      name: string
 *  }
 * ]
 */
export const getSectionListByCourseIdApi = (data: string) => {
    return request({
        url: '/api/course/getSectionListByCourseId?id=' + data,
        method: 'get',
    })
}

/**
 * 根据学生的ID查询他正在学习的课程
 * @param studentId 学生的ID
 * @returns 返回一个课程的数组
 *
 * 返回的课程数据内容：
 * {
 *  id: string
 *  classificationName: string
 *  img: string
 *  author: string
 *  createTime: string
 * }
 */
export const getLearningCourseByStudentIdApi = (studentId: string) => {
    return request({
        url: '/api/course-article/course/getLearningCourseByStudentId?id=' + studentId,
        method: 'GET'
    })
}

/**
 * 根据学生的ID查询他正在学习的课程
 * @param studentId 学生的ID
 * @returns 返回一个课程的数组
 *
 * 返回的课程数据内容：
 * {
 *  id: string
 *  classificationName: string
 *  img: string
 *  author: string
 *  createTime: string
 * }
 */
export const getCollectCourseByStudentIdApi = (studentId: string) => {
    return request({
        url: '/api/course-article/course/getCollectCourseByStudentId?id=' + studentId,
        method: 'GET'
    })
}

/**
 * 创建课程
 * @param course
 */
export const createCourseApi = (course: any) => {
    return request({
        url: '/api/course-article/course/createCourse',
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        data: course
    })
}

/**
 * 根据教师ID获取课程列表
 * @param id 教师的ID
 */
export const getCourseByTeacherIdApi = (id: string) => {
    return request({
        url: '/api/course-article/course/getCourseByTeacherId?id=' + id,
        method: 'get'
    })
}

/**
 * 学生收藏课程
 * @param courseId 课程的ID
 * @param studentId 学生的ID
 */
export const addCollectCourseByStudentIdApi = (courseId: string, studentId: string) => {
    return request({
        url: '/api/course-article/course/addCollectCourseByStudentId?courseId=' + courseId + "&studentId=" + studentId,
        method: 'get'
    })
}

/**
 * 学生学习课程
 * @param courseId 课程的ID
 * @param studentId 学生的ID
 */
export const addLearningCourseByStudentIdApi = (courseId: string, studentId: string) => {
    return request({
        url: '/api/course-article/course/addLearningCourseByStudentId?courseId=' + courseId + "&studentId=" + studentId,
        method: 'get'
    })
}

/**
 * 移除学生学习课程
 * @param courseId 课程的ID
 * @param studentId 学生的ID
 */
export const removeLearningCourseByStudentIdApi = (courseId: string, studentId: string) => {
    return request({
        url: '/api/course-article/course/removeLearningCourseByStudentId?courseId=' + courseId + "&studentId=" + studentId,
        method: 'get'
    })
}

/**
 * 移除学生收藏课程
 * @param courseId 课程的ID
 * @param studentId 学生的ID
 */
export const removeCollectCourseByStudentIdApi = (courseId: string, studentId: string) => {
    return request({
        url: '/api/course-article/course/removeCollectCourseByStudentId?courseId=' + courseId + "&studentId=" + studentId,
        method: 'get'
    })
}

/**
 * 查询是否学习该课程
 * @param courseId
 * @param studentId
 */
export const judgeLearningCourseByStudentIdApi = (courseId: string, studentId: string) => {
    return request({
        url: '/api/course-article/course/judgeLearningCourseByStudentId?courseId=' + courseId + "&studentId=" + studentId,
        method: 'get'
    })
}

/**
 * 查询是否收藏该课程
 * @param courseId
 * @param studentId
 */
export const judgeCollectCourseByStudentIdApi = (courseId: string, studentId: string) => {
    return request({
        url: '/api/course-article/course/judgeCollectCourseByStudentId?courseId=' + courseId + "&StudentId=" + studentId,
        method: 'get'
    })
}

/**
 * 根据课程ID查询课程信息
 * @param courseId 课程ID
 * @returns 返回整个课程信息
 */
export const getCourseInfoById = (courseId: string) => {
    return request({
        url: '/api/course-article/course/getCourseDetailById?id=' + courseId,
        method: 'GET'
    })
}

export const getRateByCourseIdApi = (courseId: string) => {
    return request({
        url: '/api/course-article/course/getRateByCourseId?courseId=' + courseId,
        method: 'GET'
    })
}

export const setRateByCourseIdApi = (courseId: string, rate: number) => {
    return request({
        url: '/api/course-article/course/setRateByCourseId?courseId=' + courseId + "&rate=" + rate,
        method: 'GET'
    })
}

export const getRecommendedCourseApi = (studentId: string) => {
    return request({
        url: '/api/course-article/course/getRecommendedCourse?studentId=' + studentId,
        method: 'GET'
    })
}

export const getMostSectionClicksApi = (courseId: string) => {
    return request({
        url: '/api/course-article/course/getMostSectionClicks?courseId=' + courseId,
        method: 'GET'
    })
}