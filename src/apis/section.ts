import request from '@/utils/request'

//######################################################################
/******************************* 章节接口 *******************************/

/**
 * 获取分类列表接口
 * @returns 分类列表数组
 * 返回的内容为两个：
 * {
 *  id: string
 *  name: string
 * }
 */
export const createSectionApi = (section: any) => {
    return request({
        url: '/api/course-article/section/addSection',
        method: 'POST',
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        data: section
    })
}

export const getSectionByCourseIdApi = (id: string) => {
    return request({
        url: '/api/course-article/section/getSectionByCourseId?id='+id,
        method: 'GET'
    })
}

export const getClicksBySectionIdApi = (id: string) => {
    return request({
        url: '/api/course-article/section/getClicksBySectionId?sectionId=' + id,
        method: 'GET'
    })
}

export const increaseClicksApi = (id: string) => {
    return request({
        url: '/api/course-article/section/increaseClicks?sectionId=' + id,
        method: 'GET'
    })
}