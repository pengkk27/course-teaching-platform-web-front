import request from '@/utils/request'

//######################################################################
/******************************* 分类接口 *******************************/

/**
 * 获取分类列表接口
 * @returns 分类列表数组
 * 返回的内容为两个：
 * {
 *  id: string
 *  name: string
 * }
 */
export const getCategoryListApi = () => {
    return request({
        url: '/api/tag-classification/category/getAllCategory',
        method: 'get'
    })
}