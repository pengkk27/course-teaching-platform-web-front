import request from '@/utils/request'

//######################################################################
/******************************* 讲师接口 *******************************/

/**
 * 根据讲师信息登陆讲师账号
 * @param data 学生登陆的信息
 * {
 *  phone: string
 *  password: string
 * }
 * @returns 返回是否登陆成功的信息
 * {
 *  id: string
 *  username: string
 * }
 */
export const teacherLoginApi = (data: any) => {
    return request({
        url: '/customer/teacher/teacherLogin',
        method: 'POST',
        data
    })
}

/**
 * 获取所有的讲师列表，用于首页搜索请求
 * @returns 讲师的列表
 * 返回的讲师的数据内容：
 * {
 *  id: string              讲师ID
 *  name: string            讲师姓名
 *  image: string           讲师头像
 *  describe: string        讲师描述
 *  course: Array<string>   讲师代表课程
 * }
 */
export const getAllTeacherApi = () => {
    return request({
        url: '/api/user/getAllTeacher',
        method: 'get'
    })
}

/**
 * 根据讲师的ID查询讲师的信息，用于点击查看讲师详情页面请求
 * @param id 讲师ID
 * @returns 返回讲师的除密码外的完整信息
 * {
 *
 * }
 */
export const getTeacherByIdApi = (id: string) => {
    return request({
        url: '/api/user/getInfoById?id=' + id,
        method: 'get',
    })
}
