import request from '@/utils/request'

//######################################################################
/******************************* 评论接口 *******************************/

/**
 * 根据课程ID获取该课程的所有评论
 * @param data 课程的ID和评论和评论的类型
 * @returns 课程评论的列表
 */
export const getCommentByCourseIdApi = (courseId: string) => {
    return request({
        url: '/api/comment/queryCommentByCourseId?id=' + courseId,
        method: 'GET',
    })
}

/**
 * 根据课程ID获取该文章的所有评论
 * @param data 文章的ID和评论和评论的类型
 * @returns 文章评论的列表
 */
export const getCommentByArticleIdApi = (articleId: string) => {
    return request({
        url: '/api/comment/queryCommentByArticleId?id=' + articleId,
        method: 'GET'
    })
}
/**
 * 添加评论
 * @param data 添加的评论的数据 
 * @returns 正常的返回数据
 */
export const addCommentApi = (data: object) => {
    return request({
        url: '/api/comment/addComment',
        method: 'post',
        data
    })
}

/**
 * 根据学生ID获取该学生的所有评论
 * @param studentId 学生的ID
 * @returns 返回评论的数组
 */
export const getCommentByStudentIdApi = (studentId: string) => {
    return request({
        url: '/api/comment/queryAllByStudentId?studentId=' + studentId,
        method: 'GET'
    })
}

export const deleteArticleCommentByIdApi = (data: string) => {
    return request({
        url: `/api/comment/removeArticleComment?id=` + data,
        method: 'get',
    })
}

export const judgeCommentLegalApi = (comment: object) => {
    return request({
        url: '/api/comment/judgeCommentLegal',
        method: 'POST',
        data: comment
    })
}