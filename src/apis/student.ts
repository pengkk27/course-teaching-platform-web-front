import request from '@/utils/request'

//######################################################################
/******************************* 学生接口 *******************************/

/**
 * 根据学生信息登陆学生账号
 * @param data 学生登陆的信息
 * {
 *  phone: string
 *  password: string
 * }
 * @returns 返回是否登陆成功的信息
 * {
 *  id: string
 *  username: string
 * }
 */
export const studentLoginApi = (data: any) =>{
    return request({
        url: '/api/user/login',
        method: 'post',
        data
    })
}

/**
 * 注册新的学生，并生成默认的头像
 * @param data 学生的对象
 * @returns 正常的返回
 */
export const studentRegister = (data: any) => {
    return request({
        url: '/api/user/addUser',
        method: 'post',
        data
    })
}

/**
 * 根据学生ID获取学生信息
 * @param studentId 学生的ID
 * @returns 返回学生除密码外的所有信息
 * {
 *  id: string
 *  username: string
 *  image: string
 *  phone: string
 *  effect: string
 *  createTime: string
 *  updateTime: string
 * }
 */
export const getStudentInfoByIdApi = (studentId: string) => {
    return request({
        url: '/api/user/getInfoById?id=' + studentId,
        method: 'get',
    })
}

/**
 * 根据学生ID修改学生信息
 * @param studentInfo 学生的信息
 * {
 *
 * }
 * @returns 正常的返回数据
 */
export const updateStudentInfoApi = (studentInfo: object) => {
    return request({
        url: '/api/user/updateStudentInformation',
        method: 'post',
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        data: studentInfo
    })
}

export const updateStudentPassword = (user: object) => {
    return request({
        url: '/api/user/updatePwd',
        method: 'POST',
        data: user
    })
}

export const addUserApi = (user: object) => {
    return request({
        url: '/api/user/addUser',
        method: 'POST',
        data: user
    })
}

export const updateAvatar = (avatarInfo: object) => {
    return request({
        url: '/api/user/updateImage',
        method: 'post',
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        data: avatarInfo
    })
}

export const changeEffectApi = (id: string, effect: string) => {
    return request({
        url: '/api/user/changeEffect?id=' + id + '&effect=' + effect,
        method: 'post',
    })
}

export const sendEmailApi = (email: string) => {
    return request({
        url: '/api/user/getValidateCode?email=' + email,
        method: 'GET'
    })
}

export const resetPasswordApi = (email: string, code: string) => {
    return request({
        url: '/api/user/validateCode?email=' + email + '&code=' + code,
        method: 'GET'
    })
}