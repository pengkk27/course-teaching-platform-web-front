import Mock from 'mockjs'
import category from './category'
import course from './course'
import article from './article'
import teacher from './teacher'
import student from './student'
import comment from './comment'

Mock.mock('/tag-classification/category/getAllCategory', category.getCategoryList)

// ####################################################################
/****************************** 课程请求 ******************************/

/**
 * 首页受欢迎课程请求
 */
Mock.mock('/course-article/course/popular', course.getPopular)

/**
 * 获取所有课程列表请求
 */
Mock.mock('/course-article/course/getAllCourse', course.getAllClassification)

/**
 * 获取根据课程ID获取课程节
 */
Mock.mock('/course-article/course/getSectionListByCourseId', course.getSectionListByCourseId)

/**
 * 根据学生的ID查询他正在学习的课程
 */
Mock.mock('/course-article/course/getLearningCourseByStudentId', course.getLearningCourseByStudentId)

/**
 * 根据学生的ID查询他收藏的课程
 */
Mock.mock('/course-article/course/getCollectCourseByStudentId', course.getLearningCourseByStudentId)

/**
 * 根据教师ID获取该教师的课程
 */
Mock.mock('/course-article/course/getCourseByTeacherId?id=', course.getCourseByTeacherId)

// ####################################################################
/****************************** 文章请求 ******************************/

/**
 * 获取所有文章列表请求
 */
Mock.mock('/article/getAll', article.getAllArticle)

/**
 * 获取文章详情内容
 */
Mock.mock('/article/getArticleDetailById', article.getArticleDetailById)

/**
 * 根据学生ID获取收藏的文章列表请求
 */
Mock.mock('/article/getCollectArticleByStudentId', article.getAllArticle)

/**
 * 根绝教师ID获取教师的文章
 */
Mock.mock('/course-article/article/getArticleByTeacherId?id=', article.getArticleByTeacherId)

// ####################################################################
/****************************** 讲师请求 ******************************/

/**
 * 获取所有讲师列表请求
 */
Mock.mock('/teacher/getAll', teacher.getAllTeacher)

/**
 * 根据讲师ID获得讲师的信息
 */
Mock.mock('/teacher/getTeacherInfoById', teacher.getTeacherById)


// ####################################################################
/****************************** 评论请求 ******************************/

/**
 * 根据课程ID获取该课程的所有评论
 */
Mock.mock('/comment/getCommentByCourseId', comment.getCommentByCourseId)

/**
 * 根据文章ID获取该课程的所有评论
 */
Mock.mock('/comment/getCommentByArticleId', comment.getCommentByArticleId)

/**
 * 添加评论
 */
Mock.mock('/comment/addComment', comment.addComment)

/**
 * 根据学生ID获取该学生的所有评论
 */
Mock.mock('/comment/getCommentByStudentId', comment.getCommentByStudentId)

// ####################################################################
/****************************** 学生请求 ******************************/

/**
 * 学生登陆
 */
Mock.mock('/student/login', student.studentLogin)

/**
 * 获取学生信息
 */
Mock.mock('/student/getInfo', student.getStudentInfoById)