import Mock from 'mockjs'

const articleList = Mock.mock({
    'data|23': [{
        'id|+1': 1,
        title: '计算机导论',
        author: '@cname',
        tags: ['计算机基础', '导论', '入门'],
        createTime: '@datetime(yyyy-MM-dd)',
    }]
})

const text = `# Spring Boot 整合 Mybatis

## 1、导入依赖


导入依赖的时候，需要根据自己所使用的Spring Boot和MySQL的版本而定。

## 2、创建数据库

我这里使用的是MySQL数据库。

首先创建一个mybatis_learn的数据库。然后创建一个student的表。 



对应的，要实现一个Java的实体类，来对应数据库的表。

法和set方法
}


## 3、配置文件


# 数据源配置
spring.datasource.druid.type=com.alibaba.druid.pool.DruidDataSource
spring.datasource.druid.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.druid.url=jdbc:mysql://localhost:3306/mybatis_learn?serverTimezone=UTC
spring.datasource.druid.username=root
spring.datasource.druid.password=123456

# mybatis 配置
# type aliases package是指定对应数据库实体类的包
mybatis.type-aliases-package=com.example.spring_boot_mybatis.entity
# 这里指定对应的xml文件
mybatis.mapper-locations=classpath:mapper/*.xml


## 4、编写各个文件

创建如图结构的各个包和文件。

### （1）StudentMapper

这个接口是对应Mybatis的xml文件的接口，其中的方法对应每一种操作。


### （2）StudentService和StudentServiceImpl


### （3）StudentMapper.xml

这个文件中就要写SQL的各种语句

其中开头的格式都是固定的。

namespace要对应写入上面mapper包中要实现的Mapper接口的类名。

在select语句中的id则要对应其中的方法名。

### （4）编写Controller进行测试


controller编写了一个查询接口，只要传入id就能返回数据库中的内容。

`
const articleDetail = Mock.mock({
    title: 'Spring Boot 整合 Mybatis',
    author: '@cname',
    createTime: '@datetime(yyyy-MM-dd)',
    tag: ['Java', 'C', 'Python'],
    content: text
})

const teacherArticleList = Mock.mock({
    'result|7': [{
        'id|+1': 100000000,
        articleName: '计算机的初始发展',
        tagName: ['扩展知识', '基础'],
        createTime: '@datetime(yyyy-MM-dd)'
    }]
})



export default {
    getAllArticle: () => {
        return {
            code: 200,
            data: articleList,
            msg: '操作成功'
        }
    },
    getArticleDetailById: (id: string) => {
        console.log(id)
        return {
            code: 200,
            data: articleDetail,
            msg: '操作成功'
        }
    },
    getArticleByTeacherId: (id: string) => {
        return {
            code: 200,
            ...teacherArticleList,
            message: '操作成功'
        }
    }
}