export default {
    getCategoryList: () => {
        return {
            code: 200,
            result: [
                {
                  id: '123',
                  classificationName: '前端开发'
                },
                {
                  id: '123',
                  classificationName: '后端开发'
                },
                {
                  id: '123',
                  classificationName: '移动开发'
                },
                {
                  id: '123',
                  classificationName: '计算机基础'
                },
                {
                  id: '123',
                  classificationName: '前言技术'
                },
                {
                  id: '123',
                  classificationName: '运维测试'
                },
                {
                  id: '123',
                  classificationName: '其他方向'
                },
              ],
            msg: '操作成功'
        }
    }
}