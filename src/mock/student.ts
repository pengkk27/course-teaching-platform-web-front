import Mock from 'mockjs'


export default {
    studentLogin: (loginInfo: object) => {
        console.log(loginInfo)
        return {
            code: 200,
            data: {
                id: '123456789',
                username: 'username'
            },
            msg: '操作成功'
        }
    },
    getStudentInfoById: (id: string) => {
        return {
            code: 200,
            data: {
                id: '12312312',
                username: '@cname',
                image: 'https://img0.baidu.com/it/u=1355458119,1161929395&fm=253&app=120&size=w931&n=0&f=JPEG&fmt=auto?sec=1708102800&t=bc7d6c3cc3e2a6ada289b055e509e914',
                phone: '12345678901',
                effect: '',
                createTime: '@datetime(yyyy-MM-dd)',
                updateTime: '@datetime(yyyy-MM-dd)',
            },
            msg: '操作成功'
        }
    }
}