import Mock from 'mockjs'

const teacherList = Mock.mock({
    'data|23': [{
        'id|+1': 12203010203,
        name: '@cname',
        image: 'https://img0.baidu.com/it/u=1355458119,1161929395&fm=253&app=120&size=w931&n=0&f=JPEG&fmt=auto?sec=1708102800&t=bc7d6c3cc3e2a6ada289b055e509e914',
        course: ['计算机基础', '计算机网络', '零基础轻松学Java'],
        describe: '这是一个很好的计算机好老师，有着多年的教学经验，深受同学喜爱',
    }]
})

export default {
    getAllTeacher: () => {
        return {
            code: 200,
            data: teacherList,
            msg: '操作成功'
        }
    },
    getTeacherById: (id: string) => {
        console.log(id)
        return {
            code: 200,
            data:{
               id: '1123',
               name: '@cname',
               image: 'https://img0.baidu.com/it/u=1355458119,1161929395&fm=253&app=120&size=w931&n=0&f=JPEG&fmt=auto?sec=1708102800&t=bc7d6c3cc3e2a6ada289b055e509e914',
               course: ['计算机基础', '计算机网络', '零基础轻松学Java', '计算机软件'],
               describe: '这是一个很好的计算机好老师，有着多年的教学经验，深受同学喜爱',
            },
            msg: '操作成功'
        }
    }
}