import Mock from 'mockjs'

const classificationList = Mock.mock({
    'result|23': [{
        'id|+1': 1,
        courseName: '计算机导论',
        courseImg: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
        teacherName: '@cname',
        classificationName: '计算机基础',
        tagName: ['计算机基础', '导论', '入门'],
        courseDescription: Mock.Random.csentence(),
    }]
})

const courseList = Mock.mock({
    'result|7': [{
        'id|+1': 100000001,
        courseName: '计算机系统概论',
        classificationName: '计算机基础',
        courseImg: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
        courseDescription: '这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,这是一个非常好的计算机系统概论课程，可以帮助你很快掌握计算机的基础,',
        tagName: ['计算机基础', '概论', '基础理论'],
        createTime: '@datetime(yyyy-MM-dd)'
    }]
})

export default {
    getPopular: () => {
        return {
            code: 200,
            result: [
                {
                    id: '1',
                    courseName: '零基础轻松学Java',
                    courseImg: '计算机基础',
                    courseTeacher: '123',
                    teacherName: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '2',
                    courseName: '第一行代码 Android',
                    courseImg: '计算机基础',
                    courseTeacher: 'pengkk27',
                    teacherName: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '3',
                    courseName: '程序设计基础（C++）',
                    courseImg: '计算机基础',
                    courseTeacher: 'pengkk27',
                    teacherName: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '4',
                    courseName: '计算机算法设计与分析',
                    courseImg: '计算机基础',
                    courseTeacher: 'pengkk27',
                    teacherName: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '5',
                    courseName: 'Head First 设计模式',
                    courseImg: '计算机基础',
                    courseTeacher: 'pengkk27',
                    teacherName: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '6',
                    courseName: '大话数据结构',
                    courseImg: '计算机基础',
                    courseTeacher: 'pengkk27',
                    teacherName: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '7',
                    courseName: '笨办法学Python3',
                    courseImg: '计算机基础',
                    courseTeacher: 'pengkk27',
                    teacherName: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '8',
                    courseName: '计算机系统概论',
                    courseImg: '计算机基础',
                    courseTeacher: 'pengkk27',
                    teacherName: 'pengkk27',
                    createTime: '2020-01-11'
                },

            ],
            msg: '操作成功'
        }
    },
    getAllClassification: () => {
        return {
            code: 200,
            ...classificationList,
            msg: '操作成功'
        }
    },
    getSectionListByCourseId: (id: string) => {
        console.log('课程ID: ' + id)
        return {
            code: 200,
            data: [
                {
                    id: 1,
                    name: '第一节'
                },
                {
                    id: 2,
                    name: '第二节'
                },
                {
                    id: 3,
                    name: '第三节'
                },
                {
                    id: 4,
                    name: '第四节'
                },
            ],
            msg: '操作成功'
        }
    },
    getLearningCourseByStudentId: () => {
        return {
            code: 200,
            data: [
                {
                    id: '1',
                    courseName: '课程1',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '2',
                    courseName: '课程2',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '3',
                    courseName: '课程3',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '4',
                    courseName: '课程4',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '5',
                    courseName: '课程5',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '6',
                    courseName: '课程6',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '7',
                    courseName: '课程7',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '8',
                    courseName: '课程8',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '11',
                    courseName: '课程11',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '21',
                    courseName: '课程21',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '31',
                    courseName: '课程31',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '41',
                    courseName: '课程41',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '51',
                    courseName: '课程51',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '61',
                    courseName: '课程61',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '71',
                    courseName: '课程71',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
                {
                    id: '81',
                    courseName: '课程81',
                    img: 'https://shadow.elemecdn.com/app/element/hamburger.9cf7b091-55e9-11e9-a976-7f4d0b07eef6.png',
                    author: 'pengkk27',
                    createTime: '2020-01-11'
                },
            ],
            msg: '操作成功'
        }
    },
    getCourseByTeacherId: () => {
        return {
            code: 200,
            ...courseList,
            message: '操作成功'
        }
    }
}