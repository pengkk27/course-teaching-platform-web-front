import Mock from 'mockjs'

const commentList = Mock.mock({
    'data|7': [{
        'id|+1': 1,
        commentContent: '这个课程非常的好',
        username: '@cname',
        createTime: '@datetime(yyyy-MM-dd)',
    }]
})

const allInfoCommentList = Mock.mock({
    'data|23': [{
        'id|+1': 1,
        commentContent: '这个课程非常的好',
        'user|+1': 1234456,
        type: '1',
        typeId: '123123213',
        createTime: '@datetime(yyyy-MM-dd)',
    }]
})

export default {
    getCommentByCourseId: () => {
        return {
            code: 200,
            data: commentList,
            msg: '操作成功'
        }
    },
    getCommentByArticleId: () => {
        return {
            code: 200,
            data: commentList,
            msg: '操作成功'
        }
    },
    addComment: () => {
        return {
            code: 200,
            data: '',
            msg: '操作成功'
        }
    },
    getCommentByStudentId: () => {
        return {
            code: 200,
            data: allInfoCommentList,
            msg: '操作成功'
        }
    }
}